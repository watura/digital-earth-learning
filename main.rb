# -*- coding: utf-8 -*-
require 'sinatra'
require 'sequel'
require 'erubis'

require "./models"
require "./form2csv_new"
#
# Redirect しているところでflashを設定してメッセージを表示できるようにする
#

use Rack::Session::Cookie,
:expire_after => 15552000,
:secret => "Digital-Earth-Learning-2009-ruby"
set :port, 12345

# 手抜きのため以下のアドレスにアクセスするとカテゴリが作られる　アクセスすればした数だけ同じカテゴリができる.
#

get "/cat" do
  Category.create(:name  => "カテゴリ１")
  Category.create(:name  => "カテゴリ２")
  Category.create(:name  => "カテゴリ３")
  Category.create(:name  => "カテゴリ４")
  p Category.all
  puts "hello World"
end

get '/' do
  login?
  @rows = User.filter(:id => session[:user]).first.rows
  @tours = User.filter(:id => session[:user]).first.tours
  
  erb :index  
end

#
# ツアーを作る
#

get '/createRow' do
  # post addRowに送るためのフォームを表示する
  erb :createRow
end

post '/createRow' do
  # Rowを追加する
  login?
  row = Row.create(:title => params[:title],
                   :address => params[:address],
                   :latitude => params[:latitude],
                   :longitude => params[:longitude],
                   :scale => params[:scale]
                   )
  User.where(:id => session[:user]).first.add_row row
  
  redirect '/'
end

get '/createTour' do
  # 登録してあるRow一覧が表示される そこからツアーに入れるrowを選ぶ
  # ツアーの カテゴリ 名前 (タグ) を決める (現状なし)
  login?
  @rows = User.filter(:id => session[:user]).first.rows
  @categories  =  Category.all
  erb :createTour
end
#
post '/createTour' do
  login?
  #  ツアーをDBに登録する DBにはKMLのデータも入れておく  
  diel = DiEL.new
  tour = Tour.create(:title => params[:title],
                     :description => params[:description])
  params[:rows].each do |row|
    arow = Row.filter(:id => row.to_i).first
    tour.add_row arow
    diel.addList(arow[:title],arow[:address],arow[:latitude],arow[:longitude],arow[:scale],arow[:url])
  end
    
  User.filter(:id => session[:user]).first.add_tour tour
  cat  = Category.filter(:id  => params[:category].to_i).first
  cat.add_tour tour
  tour.kml = diel.getKML
  tour.save
  puts tour.kml
  redirect '/'
end

get  '/editTour/:id' do
  login?
  @rows = User.filter(:id => session[:user]).first.rows
  @tour = Tour.filter(:id => params[:id]).first
  if @tour == nil
    redirect "/"
  end
  p @tour.category

  erb :editTour
  # 基本的にはcreateTourと同じではあるが最初から項目の中身が表示されている．  
end

post '/editTour' do
  login?
  puts " ---- editTour---"
  #  ツアーをDBに登録する DBにはKMLのデータも入れておく
  tour = Tour.filter(:id => params[:id]).first
  diel = DiEL.new
  tour.title = params[:title]
  tour.description = params[:description]
  tour.remove_all_rows
  params[:rows].each do |row|
    arow = Row.filter(:id => row.to_i).first
    tour.add_row arow
    diel.addList(arow[:title],arow[:address],arow[:latitude],arow[:longitude],arow[:scale],arow[:url])
  end
  tour.kml = diel.getKML
  tour.save
  redirect "/"
end

get '/editRow/:id' do
  @row = Row.filter(:id => params[:id],:user_id => session[:user]).first
  if @row == nil
    redirect "/"
  end
  erb :editRow
end

post '/editRow' do
  row = Row.filter(:id => params[:id],:user_id => session[:user]).first
  if row == nil
    redirect "/"
  end
  row.update(:title => params[:title],
             :address => params[:address],
             :latitude => params[:latitude],
             :longitude => params[:longitude],
             :scale => params[:scale]
             )  
  redirect "/"
end


#
# 個人ポータル
#

get '/my' do
  # 自分の作ったツアー 登録したrowが表示される 
  login?
end

get '/my/account' do 
  # アカウント情報を編集
  login?
  @account = User.filter(:id => session[:user])
end

=begin
get '/my/tour' do
  # 自分の作成したtour一覧 編集可能にしたい
  login?
  p @tours = DB[:users].filter(:id => session[:user])[:tours]
end
=end

##
# ツアーを参照する
##

get '/tours' do
  # Tour一覧
  login?
  @tours = Tour.all
  erb :tours
end

get '/tours/category/:categoryName' do 
  # :categoryNameに含まれるツアー一覧
  login?
  @tours = Category.filter(:name => params[:categoryName]).first.tours
end


get '/tours/user/:name' do
  # あるユーザの作ったtour一覧
  login?
  user = User.filter(:name => params[:name]).first
  if user == nil
    redirect "/"
  end
  @tours = user.tours
  erb :tours
end

get '/tour/id/:id' do
  # ツアーの詳細ぺーじ
  @tours = Tour.filter(:id => params[:id])
end

get '/tour/id/:id/kml' do
  # tourのKMLダウンロード
  @kml = Tour.filter(:id => params[:id])[:kml]
end

post '/tour/id/:id/comment' do
  # コメントを投稿
end

##
# Authentication
##

get '/login' do
  erb :login
end

post '/login' do
  id = params[:id]
  passwd = params[:passwd]
  p id,passwd
  if authentication(id,passwd) == false
    redirect "/login"
  end
  redirect "/"
end

get '/logout' do
  "LOGOUT"
  session[:user]= nil
  redirect '/login'
end

get '/create' do
  erb :create
end

post '/create' do
 "CREATE POST"
  id = params[:id]
  passwd = params[:passwd]
  name = params[:name]
  mail = params[:mail]
  
  if create_user(id,passwd,name,mail) == false
    return "登録に失敗しました<br> <a href=\"/create\">もどる"  
  end
  redirect "/login"
end

helpers do

  def login?
    if session[:user] == nil
      redirect "/login"
    end 
    if User.filter(:id  => session[:user]).count != 1
        session[:user]  = ""
        redirect "/login"
    end
  end
  
  def authentication(id,passwd)
    user = User.filter(:user_id => id).first
    return false if user == nil
    if user[:password] == passwd.crypt(user[:password])
      session[:user] = user[:id]
      return true
    else
      return false
    end
  end 
  
  def create_user(id,passwd,name,mail)
    salt = [rand(64),rand(64)].pack("C*").tr("\x00-\x3f","A-Za-z0-9./")
    passwd = passwd.crypt(salt)
    tmp =User.filter(:user_id => id).count  # user_id already taken
    return false if tmp >0
    User.create(:user_id => id,:password => passwd,:name => name,:mail => mail)
  end
end

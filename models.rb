# -*- coding: utf-8 -*-
require "sequel"

DB = Sequel.sqlite('diel.db')

class Tour < Sequel::Model
  many_to_one :user
  many_to_many :rows  
  one_to_many :comments
  many_to_one :category
end

class Row < Sequel::Model
  many_to_one :user
  many_to_many :tours
end

class User < Sequel::Model
  one_to_many :tours
  one_to_many :rows
end
class Comment < Sequel::Model
  many_to_one :user
  many_to_one :tour
end
class Category < Sequel::Model
  one_to_many :tours
end


DB.create_table? :tours do
  primary_key :id
  foreign_key :user_id, :users
  foreign_key :category_id, :categories
  
  String :title
  Text :description # 説明
  Integer :grade # 評価
  Text :kml
end

DB.create_table? :rows do
  primary_key :id
  foreign_key :user_id, :users 
  foreign_key :tour_id, :tours

  Integer :grade # 評価
  String :title
  String :address
  String :latitude
  String :longitude
  String :scale
end

DB.create_table? :rows_tours do
  foreign_key :tour_id, :tours
  foreign_key :row_id, :rows
end


DB.create_table? :users do
  primary_key :id
  
  String :user_id
  String :password
  String :name
  String :mail
end

DB.create_table? :comments do
  primary_key :id
  foreign_key :user_id, :users
  foreign_key :tour_id, :tours

  String :message
end

DB.create_table? :categories do
  primary_key :id
  String :name
end

DB.create_table? :tours_categories do
  foreign_key :tour_id, :tours
  foreign_key :category_id, :categories
end


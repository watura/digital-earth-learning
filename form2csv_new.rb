# -*- coding: utf-8 -*-

# 出力はkmlだけでいい?
# webform->kmlという実装にする
# form2csv->csv2kmlをいっしょくたにするかんじ
# ただしメソッドとしては別にする予定なので途中でcsvを取り出すことは可能になるはず

class DiEL
  def initialize
    @csv=""#ID,title,address,lat,lon,url,scale\n"
    @kml=Array.new
    @title = Array.new
    @place = Array.new    
  end
  
  def addList(title,address,latitude,longitude,scale,url = "")
    if title.to_s == ""|| address.to_s == ""||  latitude.to_s == "" || 
        longitude.to_s == ""|| scale.to_s == ""
      return -1
    end

    # @csvにエントリーを追加. \nの数=IDとなるのでこれでいいはず
    @csv <<  @csv.scan("\n").size.to_s + "," + title.to_s + "," + address.to_s + "," + latitude.to_s + "," + longitude.to_s + "," + scale.to_s+ "" + url.to_s+"\n"
    @kml = ""
  end

  def getCSV
    @csv
  end

  def getKML
    generateKML
    @kml
  end

  def generateKML
    place = Array.new
    tour = ["<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\">","<Document>","<name>Test</name>","<gx:Tour>","<open>1</open>","<gx:Playlist>" ]
    csvs = @csv.split("\n")
    csvs.each_with_index do |line,idx|
      rows = line.split(',')
      id = rows[0].to_s
      name = rows[1].to_s
      address = rows[2].to_s
      latitude = rows[3].to_s
      longitude = rows[4].to_s
      scale = rows[5].to_s
      
      url = rows[6].gsub(/\\/,"/") if rows[6] != nil
      scales = [0,5000,7500,10000,20000,50000 ]
      altitude = scales[scale.to_i].to_s
      place << self.make_kml(id,name,address,latitude,longitude,url)
      tour << self.make_tour(id,latitude,longitude,altitude)
    end
    tour << "</gx:Playlist>"
    tour << "</gx:Tour>"    

    @kml = tour.join + place.join + "</Document>" +"</kml>"
  end
  
  def make_kml(id,name,address,lat,lon,url = "")
    out = ""        
    out1 = " <Placemark id=\""+id+"\">\n"+
      " <name>"+name+"</name>\n"+
      " <Point>\n"+
      " <coordinates>"+lon+","+lat+"</coordinates>\n"+
      " </Point>\n"+
      " <Style id=\"sn_arrow\">\n"+
      "<IconStyle>\n"+
      "<scale>0.5</scale>\n"+
      "<Icon>\n"+
      "<href>http://maps.google.com/mapfiles/kml/shapes/arrow.png</href>\n"+
      "</Icon>\n"+
      "<hotSpot x=\"32\" y=\"1\" xunits=\"pixels\" yunits=\"pixels\"/>\n"+
      "</IconStyle>\n"+
      "</Style>\n"+
      " <description><![CDATA[\n"+
      "<table width=\"292\">\n<tr>\n<td>\n"+address+"</td>\n</tr>\n<tr>\n<td>\n"
     if url != nil
       if url =~ /.JPG$/ || url =~ /.jpg$/ || url =~ /.PNG$/ || url =~ /.png$/ || url =~ /.BMP$/ || url =~ /.bmp$/ || url =~ /.JPEG$/ || url =~ /.jpeg$/ || url =~ /.GIF$/ || url =~ /.gif$/
         out2 = "<img src=\"" + url + "\"/>"
         
       else
         out2 = "<object height=\"300\" width=\"350\">\n"+
           " <description>"+address+"</description>\n"+
           "<param value=\""+url+"\" name=\"movie\">\n"+
           "<param value=\"transparent\" name=\"wmode\">\n"+
           "<embed wmode=\"transparent\" type=\"application/x-shockwave-flash\" src=\""+url+"\" height=\"300\" width=\"350\">\n"+
           "</object>\n"
       end        
     end
    
    out3 = "</td>\n</tr>\n</table>"+
      "]]></description>\n"+
      " </Placemark>"        
    out = out1
    out = out + out2 if out2 != nil
    out = out + out3
    out = out.to_s    
    return out        
  end
  
  def make_tour(id,lat,lon,altitude)            
        tourcontents = ""        
        tourcontents = "<gx:FlyTo>\n"+        
          "<gx:duration>10.0</gx:duration>\n<LookAt>\n"+    
          "<longitude>"+lon+"</longitude>\n"+
          "<latitude>"+lat+"</latitude>\n"+
          "<altitude>0</altitude>\n"+
          "<range>"+altitude+"</range>\n"+
          "<tilt>0</tilt>\n"+
          "<heading>0</heading>\n"+
          "</LookAt>\n</gx:FlyTo>\n"+
          "<gx:AnimatedUpdate>\n<gx:duration>0.0</gx:duration>\n"+    
          "<Update>\n<targetHref/>\n<Change>\n"+    
          "<Placemark targetId=\""+id+"\">\n"+
          "<gx:balloonVisibility>1</gx:balloonVisibility>\n"+
          "</Placemark>\n</Change>\n</Update>\n</gx:AnimatedUpdate>\n"+
          "<gx:Wait>\n<gx:duration>5.0</gx:duration>\n</gx:Wait>\n"+
          "<gx:AnimatedUpdate>\n<gx:duration>0.0</gx:duration>\n"+    
          "<Update>\n<targetHref/>\n"+    
          "<Change>\n<Placemark targetId=\""+id+"\">\n"+
          "<gx:balloonVisibility>0</gx:balloonVisibility>\n"+
          "</Placemark>\n</Change>\n</Update>\n</gx:AnimatedUpdate>\n"+        
          "<gx:Wait>\n<gx:duration>1.0</gx:duration>\n</gx:Wait>"        
    return tourcontents
  end  
end


#diel = DiEL.new
 
#diel.addList("東京大空襲", "東京都台東区上野","35.65824208","139.7456314","http://www.youtube.com/v/0N3LB6fqWak","4")
#puts diel.getKML
